<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\StripeController;
use App\Http\Controllers\SubscriptionController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('checkout',[App\Http\Controllers\CheckoutController::class, 'checkout']);
Route::post('checkout',[App\Http\Controllers\CheckoutController::class, 'afterPayment'])->name('checkout.credit-card');

Route::get('stripe', [App\Http\Controllers\StripeController::class, 'stripe']);
Route::post('stripe', [App\Http\Controllers\StripeController::class, 'stripePost'])->name('stripe.post');

Route::get('plans', [App\Http\Controllers\SubscriptionController::class, 'index'])->name('plans');
Route::get('/payments', [App\Http\Controllers\PaymentController::class, 'index'])->name('payments');
Route::post('/payments',[App\Http\Controllers\PaymentController::class, 'store'])->name('payments.store');
Route::get('/subscriber',[App\Http\Controllers\PaymentController::class, 'show'])->name('subscriber.show');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
