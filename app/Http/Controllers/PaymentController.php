<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Plans;
use App\Http\Controllers\Controller;
use Session;
use Carbon\Carbon;
use Auth;


class PaymentController extends Controller
{
    public function index()
    {
    	$data = [
    		'intent' => auth()->user()->createSetupIntent()
    	];
    	// echo '<pre>';print_r($data);exit();
    	return view('subscriptions.payment')->with($data);
    }

    public function store(Request $request)
    {
    	// print_r($request->all());exit();
    	$this->validate($request,[
    		'token' => 'required'
    	]);

    	$plan = Plans::where('identifier', $request->plan)
            ->orWhere('identifier', 'basic')
            ->first();
        $user = Auth::user();
        $subscription = $request->user()->newSubscription($request->name, $plan->stripe_id)->create($request->token);
       	// dd($subscription);
       	$stripe = new \Stripe\StripeClient('sk_test_51KGzdqSAJaGQBNW5Pxg6CHDohb53ebgF3EomvQSGiPlsYAJg5vO7XW1PSVFkTvuqhvW5YgAAzAzN8QpWJ0cfgI2Z00L0T5eC4T');
		$cu = $stripe->subscriptions->retrieve($subscription->stripe_id ,[]);
	
		$subscription->trial_ends_at  = $cu->trial_end;
		$subscription->ends_at  = $cu->current_period_end;
		$subscription->save();
	 	// echo '<pre>';print_r($subscription);exit();
        Session::flash('success', 'subscriptions successful!');
        return back()->with('subscription');
    }

    public function show()
    {
		$stripe = new \Stripe\StripeClient('sk_test_51KGzdqSAJaGQBNW5Pxg6CHDohb53ebgF3EomvQSGiPlsYAJg5vO7XW1PSVFkTvuqhvW5YgAAzAzN8QpWJ0cfgI2Z00L0T5eC4T');
		$customer = $stripe->customers->all(['limit' => 3]);
		// echo '<pre>';print_r($customer);exit();
		return view('subscriptions.show',compact('customer'));
    }
}
