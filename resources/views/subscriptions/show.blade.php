@extends('layouts.app')

@section('content')
<div class="container">
	<table class="table table-hover">
		  <thead>
		    <tr>
		      <th scope="col">No</th>
		      <th scope="col">Name</th>
		      <th scope="col">Email</th>
		      <th scope="col">Balance</th>
		    </tr>
		  </thead>
		   @php $i=1;@endphp
		  @foreach($customer as $cu)
		  <tbody>
		    <tr>
		      <th>{{$i}}</th>
		      <td>{{$cu->name}}</td>
		      <td>{{$cu->email}}</td>
		      <td>{{$cu->balance}}</td>
		      @php $i++;@endphp
		    </tr>
		</tbody>
		@endforeach
	</table>
</div>
@endsection