@extends('layouts.app')

@section('content')
     @php
        $stripe_key = 'pk_test_51KGzdqSAJaGQBNW5M1Te5ENQJlzVfNoxxxM7gBlpB9HaOEcdc7P5QNuuAImezpu3gJfGl6kTF7gyR676G4aqBFDr00oM3YyBej';
    @endphp
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Checkout page') }}</div>

                <div class="card-body">
                    @if (Session::has('success'))
                        <div class="alert alert-success text-center">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <p>{{ Session::get('success') }}</p>
                        </div>
                    @endif
                    @if (Session::has('fail'))
                         <div class="alert alert-danger text-center">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <p>{{ Session::get('fail') }}</p>
                        </div>
                    @endif
                    <form id="payment-form" action="{{ route('payments.store') }}" method="POST">
                        @csrf
                        <input type="hidden" name="plan" id="plan" value="{{ request('plan') }}">
                        <div class="form-group">
                            <label for="">Subscription Type</label>
                            <input type="text" name="name" id="card-holder-name" class="form-control" value="{{ request('plan') }}" placeholder="Name on the card">
                        </div>
                        <div class="form-group mt-2">
                            <label for="">Name</label>
                            <input type="text" name="name" id="card-holder-name" class="form-control " value="" placeholder="Name on the card">
                        </div>
                        <div class="form-group mt-2">
                            <label for="">Card details</label>
                            <div id="card-element"></div>
                        </div>

                        <button type="submit" class="btn btn-primary w-100 mt-4" id="card-button" data-secret="{{ $intent->client_secret }}">Pay</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://js.stripe.com/v3/"></script>
<script>
    const stripe = Stripe('{{ $stripe_key }}', { locale: 'en' });

    const elements = stripe.elements();
    const cardElement = elements.create('card');

    cardElement.mount('#card-element');

    const form = document.getElementById('payment-form');
    const cardBtn = document.getElementById('card-button');
    const cardHolderName = document.getElementById('card-holder-name');

    form.addEventListener('submit', async (e) => {
        e.preventDefault()

        cardBtn.disabled = true
        const { setupIntent, error } = await stripe.confirmCardSetup(
            cardBtn.dataset.secret, {
                payment_method: {
                    card: cardElement,
                    billing_details: {
                        name: cardHolderName.value
                    }
                }
            }
        )

        if(error) {
            cardBtn.disable = false
        } else {
            let token = document.createElement('input')

            token.setAttribute('type', 'hidden')
            token.setAttribute('name', 'token')
            token.setAttribute('value', setupIntent.payment_method)

            form.appendChild(token)
            // alert(token);
            // console.log(token);exit();
            // exit();
            form.submit();
        }
    })
</script>
@endsection